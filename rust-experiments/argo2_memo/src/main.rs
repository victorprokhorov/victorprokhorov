use argon2::{
    password_hash::{rand_core::OsRng, PasswordHash, PasswordHasher, PasswordVerifier, SaltString},
    Argon2,
};

// fn main() -> Result<(), Box<dyn std::error::Error>> {
fn main() -> Result<(), argon2::password_hash::Error> {
    // Err("March")?
    // Err("Error...".into())
    // `Err(Box::from("Error..."))` would also work
    let password = b"hunter42";
    let salt = SaltString::generate(&mut OsRng);
    

    jfldsahfadf;

    let argon2 = Argon2::default();

    // let password_hash = argon2.hash_password(password, &salt)?.to_string();
    let password_hash = argon2
        .hash_password(password, &salt)?
        // .map_err(|e: argon2::password_hash::Error| Err(Box::from(e))?)?
        .to_string();

    let parsed_hash = PasswordHash::new(&password_hash)?;
    assert!(Argon2::default()
        .verify_password(password, &parsed_hash)
        .is_ok());

    Ok(())
}
