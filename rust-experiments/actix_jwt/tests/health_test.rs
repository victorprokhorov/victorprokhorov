use actix_jwt::{run, Token};
use hyper::{Body, Client, Method, Request};
use std::net::TcpListener;

async fn spawn_server() -> String {
    let listener = TcpListener::bind("127.0.0.1:0").unwrap();
    let port = listener.local_addr().unwrap().port();
    let address = format!("http://127.0.0.1:{}", port);
    let server = run(listener).unwrap();
    tokio::spawn(server);
    address
}

#[tokio::test]
async fn health_route_is_success() {
    let address = spawn_server().await;

    let client = Client::new();
    let req = Request::builder()
        .method(Method::HEAD)
        .uri(&format!("{address}/health"))
        .body(Body::empty())
        .unwrap();
    let resp = client.request(req).await.unwrap();

    assert!(resp.status().is_success());
}

#[tokio::test]
async fn data_extractor() {
    let address = spawn_server().await;
    let client = Client::new();

    let resp = client
        .get(format!("{address}/users").parse().unwrap())
        .await
        .unwrap();

    assert!(resp.status().is_success());
    let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();
    assert_eq!(body, "2");
}

#[tokio::test]
async fn valid_json_invalid_user_login() {
    let address = spawn_server().await;
    let req = Request::builder()
        .method(Method::POST)
        .uri::<String>(format!("{address}/login").parse().unwrap())
        .header("content-type", "application/json")
        .body(Body::from(r#"{"name":"","pw":""}"#))
        .unwrap();
    let client = Client::builder().build_http();
    let resp = client.request(req).await.unwrap();
    assert!(resp.status().is_server_error());
    assert_eq!(resp.status(), 500);
    let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();
    assert_eq!(body, "non-existing user");
}

#[tokio::test]
async fn invalid_json_login() {
    let address = spawn_server().await;
    let req = Request::builder()
        .method(Method::POST)
        .uri::<String>(format!("{address}/login").parse().unwrap())
        .header("content-type", "application/json")
        .body(Body::from(r#""#))
        .unwrap();
    let client = Client::builder().build_http();
    let resp = client.request(req).await.unwrap();
    assert!(resp.status().is_server_error());
    assert_eq!(resp.status(), 500);
    let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();
    assert_eq!(body, "invalid json structure");
}

#[tokio::test]
async fn invalid_password_login() {
    let address = spawn_server().await;
    let req = Request::builder()
        .method(Method::POST)
        .uri::<String>(format!("{address}/login").parse().unwrap())
        .header("content-type", "application/json")
        .body(Body::from(r#"{"name":"user","pw":"admin"}"#))
        .unwrap();
    let client = Client::builder().build_http();
    let resp = client.request(req).await.unwrap();
    assert!(resp.status().is_server_error());
    assert_eq!(resp.status(), 500);
    let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();
    assert_eq!(body, "wrong password");
}

#[tokio::test]
async fn correct_password_login() {
    let address = spawn_server().await;
    let req = Request::builder()
        .method(Method::POST)
        .uri::<String>(format!("{address}/login").parse().unwrap())
        .header("content-type", "application/json")
        .body(Body::from(r#"{"name":"admin","pw":"admin"}"#))
        .unwrap();
    let client = Client::builder().build_http();
    let resp = client.request(req).await.unwrap();
    assert!(resp.status().is_success());
    assert_eq!(resp.status(), 200);
    let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();
    let parsed: Token = serde_json::from_slice(&body).unwrap();
    assert!(!parsed.0.is_empty());
}

#[tokio::test]
async fn user_refuse_access() {
    let address = spawn_server().await;
    let req = Request::builder()
        .method(Method::POST)
        .uri::<String>(format!("{address}/login").parse().unwrap())
        .header("content-type", "application/json")
        .body(Body::from(r#"{"name":"user","pw":"user"}"#))
        .unwrap();
    let client = Client::builder().build_http();
    let resp = client.request(req).await.unwrap();
    assert!(resp.status().is_success());
    assert_eq!(resp.status(), 200);
    let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();
    let t: Token = serde_json::from_slice(&body).unwrap();
    println!("{:?}", t.0);
    assert!(!t.0.is_empty());

    let req = Request::builder()
        .method(Method::POST)
        .uri::<String>(format!("{address}/admin").parse().unwrap())
        .header("content-type", "application/json")
        .header("Authorization", format!("{}{}", "Bearer ", &t.0))
        .body(Body::empty())
        .unwrap();
    let client = Client::new();
    let resp = client.request(req).await.unwrap();
    assert!(resp.status().is_server_error());
    let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();
    assert_eq!(body, "non-authorized");
}

#[tokio::test]
async fn admin_correct_access() {
    let address = spawn_server().await;
    let req = Request::builder()
        .method(Method::POST)
        .uri::<String>(format!("{address}/login").parse().unwrap())
        .header("content-type", "application/json")
        .body(Body::from(r#"{"name":"admin","pw":"admin"}"#))
        .unwrap();
    let client = Client::builder().build_http();
    let resp = client.request(req).await.unwrap();
    assert!(resp.status().is_success());
    assert_eq!(resp.status(), 200);
    let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();
    let t: Token = serde_json::from_slice(&body).unwrap();
    println!("{:?}", t.0);
    assert!(!t.0.is_empty());

    let req = Request::builder()
        .method(Method::POST)
        .uri::<String>(format!("{address}/admin").parse().unwrap())
        .header("content-type", "application/json")
        .header("Authorization", format!("{}{}", "Bearer ", &t.0))
        .body(Body::empty())
        .unwrap();
    let client = Client::new();
    let resp = client.request(req).await.unwrap();
    assert!(resp.status().is_success());
    let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();
    assert_eq!(body, "authorized");
}

#[tokio::test]
async fn authenticated_invalid_token() {
    let address = spawn_server().await;
    let req = Request::builder()
        .method(Method::POST)
        .uri::<String>(format!("{address}/authenticated").parse().unwrap())
        .header("content-type", "application/json")
        .header("Authorization", "Bearer invalid")
        .body(Body::empty())
        .unwrap();

    let client = Client::builder().build_http();

    let resp = client.request(req).await.unwrap();
    assert!(resp.status().is_server_error());
    assert_eq!(resp.status(), 500);
    let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();
    assert_eq!(body, "invalid token");
}

#[tokio::test]
async fn authenticated_missing_header() {
    let address = spawn_server().await;
    let req = Request::builder()
        .method(Method::POST)
        .uri::<String>(format!("{address}/authenticated").parse().unwrap())
        .header("content-type", "application/json")
        .body(Body::empty())
        .unwrap();

    let client = Client::builder().build_http();

    let resp = client.request(req).await.unwrap();
    assert!(resp.status().is_server_error());
    assert_eq!(resp.status(), 500);
    let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();
    assert_eq!(body, "missing header");
}

#[tokio::test]
async fn user_creation_flow() {
    let address = spawn_server().await;
    let name = r#"hunter"#;
    let pw = r#"Hunter42"#;
    let body = format!(r#"{{"name":"{}","pw":"{}"}}"#, name, pw);
    println!("{body}");
    let req = Request::builder()
        .method(Method::POST)
        .uri::<String>(format!("{address}/user").parse().unwrap())
        .header("content-type", "application/json")
        .body(Body::from(body))
        .unwrap();
    let client = Client::builder().build_http();
    let resp = client.request(req).await.unwrap();
    assert!(resp.status().is_success());
    assert_eq!(resp.status(), 200);
    let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();
    let t: Token = serde_json::from_slice(&body).unwrap();
    println!("{:?}", t.0);
    assert!(!t.0.is_empty());

    // should be a user not admin
    let req = Request::builder()
        .method(Method::POST)
        .uri::<String>(format!("{address}/admin").parse().unwrap())
        .header("content-type", "application/json")
        .header("Authorization", format!("{}{}", "Bearer ", &t.0))
        .body(Body::empty())
        .unwrap();
    let client = Client::new();
    let resp = client.request(req).await.unwrap();
    assert!(resp.status().is_server_error());
    let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();
    assert_eq!(body, "non-authorized");

    // but can be authenticated
    let req = Request::builder()
        .method(Method::POST)
        .uri::<String>(format!("{address}/authenticated").parse().unwrap())
        .header("content-type", "application/json")
        .header("Authorization", format!("{}{}", "Bearer ", &t.0))
        .body(Body::empty())
        .unwrap();

    let client = Client::builder().build_http();

    let resp = client.request(req).await.unwrap();
    assert!(resp.status().is_success());
    assert_eq!(resp.status(), 200);
    let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();
    assert_eq!(body, "authenticated");
}
