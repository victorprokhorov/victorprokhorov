mod auth;

use crate::auth::AuthorizationService;
use actix_web::dev::Server;
use actix_web::{error, web, App, Error, HttpResponse, HttpServer, Responder};
use argon2::{
    password_hash::{rand_core::OsRng, PasswordHash, PasswordHasher, PasswordVerifier, SaltString},
    Argon2,
};
use chrono::Utc;
use futures::StreamExt;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::net::TcpListener;
use std::sync::Mutex;

async fn health() -> impl Responder {
    HttpResponse::Ok().finish()
}

async fn users(data: web::Data<State>) -> HttpResponse {
    let map = data.users.lock().unwrap();
    assert!(map.contains_key(&1));
    assert_eq!(map.get(&1).unwrap().id, 1);
    assert_eq!(map.get(&1).unwrap().role, Role::Admin);
    let len = map.keys().len();
    HttpResponse::Ok().body(len.to_string())
}

#[derive(Debug, Serialize, Deserialize)]
struct Credentials {
    name: String,
    pw: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Token(pub String);

#[derive(Debug, Serialize, Deserialize, Clone)]
struct Claims {
    sub: String,
    pub role: String,
    exp: usize,
}

const MAX_SIZE: usize = 262_144;

async fn user_flow(data: web::Data<State>, creds: web::Json<Credentials>) -> HttpResponse {
    // TODO: exract to separate fns too much of the same code
    let mut map = data.users.lock().unwrap();
    let len = map.keys().len();
    let argon2 = Argon2::default();
    let new_user = User {
        name: creds.name.to_string(),
        id: len as u64 + 1,
        role: Role::User,
        pw: argon2
            .hash_password(creds.pw.as_bytes(), &SaltString::generate(&mut OsRng))
            .unwrap()
            .to_string(),
    };
    println!("{}\n{:?}", new_user.id, new_user);
    map.insert(new_user.id, new_user);
    let user = &map[&(len as u64 + 1)];
    println!("{:?}", user);

    let claims = Claims {
        sub: user.id.to_string(),
        role: user.role.into(),
        exp: Utc::now()
            .checked_add_signed(chrono::Duration::hours(24))
            .expect("valid timestamp")
            .timestamp() as usize,
    };

    let token = jsonwebtoken::encode(
        &jsonwebtoken::Header::default(),
        &claims,
        &jsonwebtoken::EncodingKey::from_secret("secret".as_ref()),
    )
    .unwrap();
    let token = Token(token);
    let token = serde_json::to_string(&token).unwrap();
    HttpResponse::Ok().body(token)
}

async fn login(data: web::Data<State>, mut payload: web::Payload) -> Result<HttpResponse, Error> {
    let mut body = web::BytesMut::new();
    while let Some(chunk) = payload.next().await {
        let chunk = chunk?;
        if (body.len() + chunk.len()) > MAX_SIZE {
            return Err(error::ErrorBadRequest("overflow"));
        }
        body.extend_from_slice(&chunk);
    }

    let credentials = serde_json::from_slice::<Credentials>(&body)
        .map_err(|_| error::ErrorInternalServerError("invalid json structure"))?;

    let map = data.users.lock().unwrap();
    match map.iter().find_map(|(k, v)| {
        if v.name == credentials.name {
            Some(k)
        } else {
            None
        }
    }) {
        Some(k) => {
            println!("=== user id:\n {:?}", k);
            let user: &User = map.get(k).unwrap();
            let parsed_hash = PasswordHash::new(&user.pw).unwrap();
            let res = Argon2::default().verify_password(credentials.pw.as_bytes(), &parsed_hash);
            match res {
                Ok(_) => {
                    let claims = Claims {
                        sub: user.id.to_string(),
                        role: user.role.into(),
                        exp: Utc::now()
                            .checked_add_signed(chrono::Duration::hours(24))
                            .expect("valid timestamp")
                            .timestamp() as usize,
                    };

                    let token = jsonwebtoken::encode(
                        &jsonwebtoken::Header::default(),
                        &claims,
                        &jsonwebtoken::EncodingKey::from_secret("secret".as_ref()),
                    )
                    .unwrap();
                    let token = Token(token);
                    let token = serde_json::to_string(&token).unwrap();
                    Ok(HttpResponse::Ok().body(token))
                }
                Err(_) => Err(error::ErrorInternalServerError("wrong password")),
            }
        }
        None => Err(error::ErrorInternalServerError("non-existing user")),
    }
}

// async fn admin(auth: Result<AuthorizationService, Error>, token: web::Json<Token>) -> HttpResponse {
async fn admin(auth: Result<AuthorizationService, Error>) -> HttpResponse {
    // println!("=== token:\n{:?}", token);
    match auth {
        Ok(auth) => {
            println!("=== claims:\n{:?}", auth);
            // just for memo
            let _a: String = Role::Admin.to_string();
            // println!("\n\n\n-------->{:?}", a);
            let adm = Role::Admin;
            let n: String = adm.into();
            // NOTE: we can impl From and Into or impl Display or write simplt match ourselves
            if auth.claims.role == n {
                return HttpResponse::Ok().body("authorized");
            }
            HttpResponse::InternalServerError().body("non-authorized")
        }
        Err(err) => HttpResponse::InternalServerError().body(err.to_string()),
    }
}

async fn authenticated(auth: Result<AuthorizationService, Error>) -> HttpResponse {
    match auth {
        Ok(_) => HttpResponse::Ok().body("authenticated"),
        Err(err) => HttpResponse::InternalServerError().body(err.to_string()),
    }
}

#[derive(Debug, PartialEq, Copy, Clone)]
enum Role {
    User,
    Admin,
}

// TODO: refactor to From<_>
impl Into<String> for Role {
    fn into(self) -> String {
        match self {
            Role::User => "user".to_string(),
            Role::Admin => "admin".to_string(),
        }
    }
}

impl std::fmt::Display for Role {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        // println!("??? {:?}", self);
        write!(f, "{:?}", self)
    }
}

#[derive(Debug)]
pub struct User {
    name: String,
    id: u64,
    role: Role,
    pw: String,
}

pub type Map = HashMap<u64, User>;

struct State {
    users: Mutex<Map>,
}

pub fn run(listener: TcpListener) -> Result<Server, std::io::Error> {
    let argon2 = Argon2::default();

    let user_admin = User {
        name: "admin".to_string(),
        id: 1,
        role: Role::Admin,
        pw: argon2
            .hash_password(b"admin", &SaltString::generate(&mut OsRng))
            .unwrap()
            .to_string(),
    };

    let user = User {
        name: "user".to_string(),
        id: 2,
        role: Role::User,
        pw: argon2
            .hash_password(b"user", &SaltString::generate(&mut OsRng))
            .unwrap()
            .to_string(),
    };

    let map: Map = HashMap::from([(user_admin.id, user_admin), (user.id, user)]);
    assert!(map.contains_key(&1));
    assert_eq!(map.get(&1).unwrap().id, 1);
    assert_eq!(map.get(&1).unwrap().role, Role::Admin);
    let parsed_hash = PasswordHash::new(&map.get(&1).unwrap().pw).unwrap();
    assert!(Argon2::default()
        .verify_password(b"admin", &parsed_hash)
        .is_ok());

    let data = web::Data::new(State {
        users: Mutex::new(map),
    });

    let server = HttpServer::new(move || {
        App::new()
            .app_data(data.clone())
            .route("/login", web::post().to(login))
            .route("/health", web::head().to(health))
            .route("/users", web::get().to(users))
            .route("/user", web::post().to(user_flow))
            .route("/authenticated", web::post().to(authenticated))
            .route("/admin", web::post().to(admin))
    })
    .listen(listener)?
    .run();

    Ok(server)
}
