use actix_jwt::run;
use std::net::TcpListener;

#[tokio::main]
async fn main() -> std::io::Result<()> {
    let address = "0.0.0.0:8080".to_string();
    let listener = TcpListener::bind(address)?;
    run(listener)?.await?;

    Ok(())
}
