use crate::{Role, State};
use actix_web::error::ErrorUnauthorized;
use actix_web::web::Data;
use actix_web::{dev, Error, FromRequest, HttpRequest};
use futures::future::{ready, Ready};
use jsonwebtoken::{decode, Algorithm, DecodingKey, Validation};
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct Claims {
    sub: String,
    pub role: String,
    exp: usize,
}

#[derive(Debug)]
pub struct AuthorizationService {
    // tuple struct nicer?
    pub claims: Claims,
}

// https://stackoverflow.com/questions/71731613/actix-web-add-data-to-request-in-middleware
impl FromRequest for AuthorizationService {
    type Error = Error;
    // type Future = Ready<Result<AuthorizationService, Error>>;
    // type Future = Ready<Result<Claims, Error>>;
    type Future = Ready<Result<Self, Self::Error>>;

    fn from_request(req: &HttpRequest, _payload: &mut dev::Payload) -> Self::Future {
        let data: Option<&Data<State>> = req.app_data();
        let map = data.unwrap().users.lock().unwrap();
        assert!(map.contains_key(&1));
        assert_eq!(map.get(&1).unwrap().id, 1);
        assert_eq!(map.get(&1).unwrap().role, Role::Admin);
        let auth = req.headers().get("Authorization");

        match auth {
            Some(auth) => {
                let str = auth.to_str().unwrap();
                let token = &str[7..];
                let secret = "secret";
                let key = secret.as_bytes();

                match decode::<Claims>(
                    token,
                    &DecodingKey::from_secret(key),
                    &Validation::new(Algorithm::HS256),
                ) {
                    // try ready(Ok())
                    // Ok(c) => ok(AuthorizationService { claims: c.claims }),
                    Ok(c) => ready(Ok(AuthorizationService { claims: c.claims })),
                    // Err(_) => err(ErrorUnauthorized("invalid token")),
                    Err(_) => ready(Err(ErrorUnauthorized("invalid token"))),
                }
            }
            // None => err(ErrorUnauthorized("missing header")),
            None => ready(Err(ErrorUnauthorized("missing header"))),
        }
    }
}
