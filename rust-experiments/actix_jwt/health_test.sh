clear

curl -w '\n' --head http://localhost:8080/health_check

curl \
  --header "Content-Type: application/json, Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VyIiwicm9sZSI6ImNvbXBhbnkiLCJleHAiOjE2NjAwMDA3OTB9.xsSehYISBi2-0AoHRzKNPi2yaY-GkYLvk1AwFivD0t0" \
  --request POST \
  --data '{"email":"email_data","name":"name_data"}' \
  http://localhost:8080/protected


