use argon2::{self, Config};

// https://cheatsheetseries.owasp.org/cheatsheets/Cryptographic_Storage_Cheat_Sheet.html
fn main() {
    // let password = b"password";
    // let salt = b"randomsalt";
    // let config = Config::default();
    // let hash = argon2::hash_encoded(password, salt, &config).unwrap();
    // println!("1. {hash}");
    // let hash1 = argon2::hash_encoded(b"password1", b"randomsalt1", &config).unwrap();
    // println!("2. {hash}");
    let enc =
        "$argon2i$v=19$m=4096,t=3,p=1$cmFuZG9tc2FsdA$UlGQVbg0LH6E4TZ5M8SMxDJ5mN0OxiWmmC2vY2A5USw";
    let matches = argon2::verify_encoded(enc, b"password").unwrap();
    assert!(matches);
    // let matches = argon2::verify_encoded(&hash1, b"password1").unwrap();
    // assert!(matches);
}
