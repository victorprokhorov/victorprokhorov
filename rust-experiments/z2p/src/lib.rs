//! lib.rs
use actix_web::{web, App, HttpResponse, HttpServer};
use std::net::TcpListener;

async fn health_check() -> HttpResponse {
    HttpResponse::Ok().finish()
}

use actix_web::dev::Server;

// We need to mark `run` as public.
// It is no longer a binary entrypoint, therefore we can mark it as async
// without having to use any proc-macro incantation.
// pub fn run(address: &str) -> Result<Server, std::io::Error> {
pub fn run(listener: TcpListener) -> Result<Server, std::io::Error> {
    let server = HttpServer::new(|| App::new().route("/health_check", web::get().to(health_check)))
        // .bind(address)?
        .listen(listener)?
        .run();
    // No .await here!
    // we ret ok now
    Ok(server)
}
