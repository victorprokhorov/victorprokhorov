use mod_use_crate_pkg_name_dbg_memo::lib_fn; // already compiled lib, here package name refers
mod another;
use crate::another::another_fn; // here crate refers to `main` crate module tree

fn main() {
    lib_fn();
    another_fn();

    for i in 0..3 {
        dbg!(i);
    }
    let a = dbg!(1 - 1);

    if a == 0 {
        return;
    }

    unreachable!("a is hardcoded"); // explicit way to check for logic errors in dev
}

#[allow(dead_code)]
fn cool() {
    let _a = todo!();
}
