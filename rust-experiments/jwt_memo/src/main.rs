use chrono::Utc;
use jsonwebtoken::{decode, encode, Algorithm, DecodingKey, EncodingKey, Header, Validation};
use serde::{Deserialize, Serialize};
use std::error::Error;

// type Result<T> = std::result::Result<T, Box<dyn Error>>;

/// Our claims struct, it needs to derive `Serialize` and/or `Deserialize`
#[derive(Debug, Serialize, Deserialize)]
struct Claims {
    sub: String,
    // company: String,
    role: String,
    exp: usize, // Expiration time (as UTC timestamp)
}

fn main() -> Result<(), Box<dyn Error>> {
    let claims = Claims {
        sub: "user".to_string(),
        role: "company".to_string(),
        exp: Utc::now()
            .checked_add_signed(chrono::Duration::hours(24))
            .expect("valid timestamp")
            .timestamp() as usize,
    };

    let token = encode(
        // HS256 signing algorithm
        // &Header::default(),
        &Header::new(Algorithm::HS256),
        &claims,
        &EncodingKey::from_secret("secret".as_ref()),
    )?;
    println!("{token}");

    let token = decode::<Claims>(
        &token,
        &DecodingKey::from_secret("secret".as_ref()),
        // &Validation::default(),
        &Validation::new(Algorithm::HS256),
    )?;
    println!("{:?}", token);

    let expect_to_fail = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VyIiwiY29tcGFueSI6ImNvbXBhbnkiLCJleHAiOjE2NTk5MDQ3MzF9.x4UQgiO-g3Q21p90bb9tGxYxNU-zQvP5MHJziOcrccc";
    let token = decode::<Claims>(
        expect_to_fail,
        &DecodingKey::from_secret("secret".as_ref()),
        // &Validation::default(),
        &Validation::new(Algorithm::HS256),
    )?;
    // Error: Error(ExpiredSignature)
    println!("{:?}", token);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn ok() -> Result<(), Box<dyn Error>> {
        let claims = Claims {
            sub: "user".to_string(),
            role: "company".to_string(),
            exp: Utc::now()
                .checked_add_signed(chrono::Duration::seconds(1))
                .expect("valid timestamp")
                .timestamp() as usize,
        };

        let token = encode(
            // HS256 signing algorithm
            // &Header::default(),
            &Header::new(Algorithm::HS256),
            &claims,
            &EncodingKey::from_secret("secret".as_ref()),
        )?;
        println!("{token}");

        let token = decode::<Claims>(
            &token,
            &DecodingKey::from_secret("secret".as_ref()),
            // &Validation::default(),
            &Validation::new(Algorithm::HS256),
        )?;
        println!("{:?}", token);

        Ok(())
    }
    #[test]
    // #[should_panic] Err not panic
    fn fail() {
        let expect_to_fail = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VyIiwiY29tcGFueSI6ImNvbXBhbnkiLCJleHAiOjE2NTk5MDQ3MzF9.x4UQgiO-g3Q21p90bb9tGxYxNU-zQvP5MHJziOcrccc";

        let token: Result<jsonwebtoken::TokenData<Claims>, jsonwebtoken::errors::Error> =
            decode::<Claims>(
                expect_to_fail,
                &DecodingKey::from_secret("secret".as_ref()),
                // &Validation::default(),
                &Validation::new(Algorithm::HS256),
            );
        // jsonwebtoken::errors::Error is private
        assert_eq!(
            token
                // return only kind wihtout error but still in result
                .map_err(|e: jsonwebtoken::errors::Error| e.into_kind())
                // unwrap result and keep err
                .unwrap_err(),
            // assert kind
            jsonwebtoken::errors::ErrorKind::ExpiredSignature
        );
    }
}
