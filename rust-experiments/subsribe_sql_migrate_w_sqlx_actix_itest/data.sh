clear

curl -w '\n' --head http://localhost:8000/health_check

curl \
  --header "Content-Type: application/json" \
  --request POST \
  --data '{"email":"email_data","name":"name_data"}' \
  http://localhost:8000/subscribe

