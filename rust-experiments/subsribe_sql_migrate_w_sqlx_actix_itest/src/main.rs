use sqlx::postgres::PgPool;
use sqlx::postgres::Postgres;
use sqlx::Pool;

use std::net::TcpListener;
use zero2prod::configuration::get_configuration;
use zero2prod::startup::run;

#[tokio::main]
async fn main() -> std::io::Result<()> {
    let configuration = get_configuration().expect("Failed to read configuration.");

    // https://docs.rs/sqlx/latest/sqlx/struct.Pool.html
    // https://docs.rs/sqlx/latest/sqlx/pool/index.html
    let connection_pool = PgPool::connect(&configuration.database.connection_string())
        .await
        .expect("Failed to connect to Postgres.");

    let address = format!("0.0.0.0:{}", configuration.application_port);
    let listener = TcpListener::bind(address)?;
    println!("{:?}", listener);

    run(listener, connection_pool)?.await?;
    Ok(())
}
