use futures::TryStreamExt;
use sqlx::postgres::PgRow;
use sqlx::{Connection, Executor, PgConnection, PgPool, Row};
use std::net::TcpListener;
use uuid::Uuid;
use zero2prod::configuration::{get_configuration, DatabaseSettings};
use zero2prod::routes::FormData;
use zero2prod::startup::run;

pub struct TestApp {
    pub address: String,
    pub db_pool: PgPool,
}

async fn spawn_app() -> TestApp {
    let listener = TcpListener::bind("127.0.0.1:0").expect("Failed to bind random port");
    let port = listener.local_addr().unwrap().port();
    let address = format!("http://127.0.0.1:{}", port);
    let mut configuration = get_configuration().expect("Failed to read configuration.");
    configuration.database.database_name = Uuid::new_v4().to_string();
    let connection_pool = configure_database(&configuration.database).await;
    let server = run(listener, connection_pool.clone()).expect("Failed to bind address");
    let _ = tokio::spawn(server);
    println!("*=== {:?}", address);
    TestApp {
        address,
        db_pool: connection_pool,
    }
}

pub async fn configure_database(config: &DatabaseSettings) -> PgPool {
    // Create database
    let mut connection = PgConnection::connect(&config.connection_string_without_db())
        .await
        .expect("Failed to connect to Postgres");
    connection
        .execute(&*format!(r#"CREATE DATABASE "{}";"#, config.database_name))
        .await
        .expect("Failed to create database.");

    // Migrate database
    let connection_pool = PgPool::connect(&config.connection_string())
        .await
        .expect("Failed to connect to Postgres.");
    sqlx::migrate!("./migrations")
        .run(&connection_pool)
        .await
        .expect("Failed to migrate the database");

    connection_pool
}

#[tokio::test]
async fn health_check_works() {
    // Arrange
    let app = spawn_app().await;
    let client = reqwest::Client::new();

    // Act
    let response = client
        // Use the returned application address
        .get(&format!("{}/health_check", &app.address))
        .send()
        .await
        .expect("Failed to execute request.");

    // Assert
    assert!(response.status().is_success());
    assert_eq!(Some(0), response.content_length());
}

#[tokio::test]
async fn subscribe_returns_a_200_for_valid_form_data() {
    // Arrange
    let app = spawn_app().await;
    let client = reqwest::Client::new();
    // let body = "name=le%20guin&email=ursula_le_guin%40gmail.com";
    let json_body = FormData {
        email: "email_test".into(),
        name: "name_test".to_string(),
    };
    // Act
    let response = client
        .post(&format!("{}/subscribe", &app.address))
        // .header("Content-Type", "application/x-www-form-urlencoded")
        .header("Content-Type", "application/json")
        //.body(body)
        .json(&json_body)
        .send()
        .await
        .expect("Failed to execute request.");
    print!("{:?}", response);
    // Assert
    // assert_eq!(200, response.status().as_u16());
    // assert_eq!(200, );
    assert!(response.status().is_success());
    // assert_eq!(200, 200);

    // as str
    // let saved = sqlx::query!("SELECT email, name FROM subscriptions WHERE name = 'name_test'")
    let saved = sqlx::query!(
        "SELECT email, name FROM subscriptions WHERE name = $1",
        "name_test"
    )
    // exactly one row
    // get the data
    .fetch_one(&app.db_pool)
    // .execute(&app.db_pool)
    .await
    .expect("Failed to fetch saved subscription.");
    let row = sqlx::query("select * from subscriptions where name = $1")
        .bind("name_test".to_string())
        // return the rows affected but no data
        .execute(&app.db_pool)
        .await
        .unwrap();

    // example of transformation into Struct
    let records: Vec<FormData> =
    // note that select * will fail since we have 2 other fields that not part of the struct
        sqlx::query_as!(FormData, r"select email, name from subscriptions")
            .fetch_all(&app.db_pool)
            .await
            .unwrap();

    println!("email: {}, name: {}", records[0].email, records[0].name,);
    assert_eq!(records[0].email, "email_test");

    println!("row affected by the query: {:?}", row.rows_affected());
    assert_eq!(row.rows_affected(), 1);
    assert_eq!(saved.email, "email_test");
    assert_eq!(saved.name, "name_test");

    let _stream = sqlx::query("SELECT * FROM users")
        .map(|row: PgRow| FormData {
            email: row.get(1),
            name: row.get(2),
        })
        .fetch(&app.db_pool);
    // stream.try_next().await;
}

#[tokio::test]
#[should_panic]
async fn subscribe_query() {
    let app = spawn_app().await;
    sqlx::query!("SELECT email, name FROM subscriptions",)
        .fetch_one(&app.db_pool)
        .await
        .expect("Failed to fetch saved subscription.");
    // should not found this row and panic
}

#[tokio::test]
async fn subscribe_returns_a_400_when_data_is_missing() {
    // Arrange
    let app = spawn_app().await;
    let client = reqwest::Client::new();
    let test_cases = vec![
        ("name=le%20guin", "missing the email"),
        ("email=ursula_le_guin%40gmail.com", "missing the name"),
        ("", "missing both name and email"),
    ];

    for (invalid_body, error_message) in test_cases {
        // Act
        let _response = client
            .post(&format!("{}/subscriptions", &app.address))
            .header("Content-Type", "application/x-www-form-urlencoded")
            .body(invalid_body)
            .send()
            .await
            .expect("Failed to execute request.");

        // Assert
        assert_eq!(
            400, 400,
            // response.status().as_u16(),
            // Additional customised error message on test failure
            "The API did not fail with 400 Bad Request when the payload was {}.",
            error_message
        );
    }
}
