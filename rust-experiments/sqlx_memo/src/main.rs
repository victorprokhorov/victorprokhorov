#![allow(dead_code)]
use futures::TryStreamExt;
use sqlx::postgres::{PgPoolOptions, PgRow};
use sqlx::{FromRow, Row};
use std::time::Instant;

// docker run --rm --name pg -p 5432:5432  -e POSTGRES_PASSWORD=welcome  postgres
// docker exec -it -u postgres pg psql
// To log all sql query out to the first terminal, in psql run:
// ALTER DATABASE postgres SET log_statement = 'all';

#[derive(Debug, FromRow)]
struct Ticket {
    id: i64,
    name: String,
}

#[tokio::main]
async fn main() -> Result<(), sqlx::Error> {
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect("postgres://postgres:welcome@localhost/postgres")
        .await?;

    let ticket_table =
        sqlx::query(r#"CREATE TABLE IF NOT EXISTS ticket (id bigserial, name text);"#)
            .execute(&pool)
            .await?;
    assert_eq!(ticket_table.rows_affected(), 0);

    let rows_count = sqlx::query("SELECT COUNT(*) FROM ticket;")
        .fetch_one(&pool)
        .await?
        // https://docs.rs/sqlx/0.3.5/sqlx/postgres/types/index.html
        .get::<i64, _>(0);
    println!("\n=== rows count:\n{:?}", rows_count);

    let row: (i64,) = sqlx::query_as("insert into ticket (name) values ($1) returning id")
        .bind("a new ticket")
        .fetch_one(&pool)
        .await?;
    println!("\n=== inserted ticket id:\n{}", row.0);

    let rows = sqlx::query("SELECT * FROM ticket").fetch_all(&pool).await?;
    let printable_rows = rows
        .iter()
        .map(|r| format!("{} - {}", r.get::<i64, _>("id"), r.get::<String, _>("name")))
        .collect::<Vec<String>>()
        .join("\n");
    println!("\n== select tickets with query:\n{}", printable_rows);

    let select_query = sqlx::query("SELECT id, name FROM ticket");
    let tickets: Vec<Ticket> = select_query
        .map(|row: PgRow| Ticket {
            id: row.get("id"),
            name: row.get("name"),
        })
        .fetch_all(&pool)
        .await?;
    println!("\n=== select tickets with query.map...:");
    tickets
        .iter()
        .for_each(|ticket: &Ticket| println!("{:?}", ticket));

    let select_query = sqlx::query_as::<_, Ticket>("SELECT id, name FROM ticket");
    let tickets: Vec<Ticket> = select_query.fetch_all(&pool).await?;
    println!("\n=== select tickets with query.map...:");
    tickets
        .iter()
        .for_each(|ticket: &Ticket| println!("{:?}", ticket));

    let id = 1;
    let row = sqlx::query_as::<_, Ticket>("SELECT id, name FROM ticket WHERE id = $1")
        .bind(id)
        .fetch_one(&pool)
        .await;
    if let Ok(row) = row {
        println!("\n=== query_as:\n{:?}", row);
    }

    let row = sqlx::query_as::<_, Ticket>("DELETE FROM ticket WHERE id = 2")
        .bind(id)
        .fetch_optional(&pool)
        .await;
    println!("\n=== fetch_optional:\n{:?}", row);

    if rows_count > 10 {
        let row = sqlx::query("DELETE FROM ticket WHERE name = 'a new ticket'")
            .execute(&pool)
            .await?;
        println!("\n=== removed:\n{:?}", row.rows_affected());
    }

    let mut stream = sqlx::query_as::<_, Ticket>("SELECT id, name FROM ticket").fetch(&pool);
    println!("\n=== rows from stream:\n");
    let start = Instant::now();
    while let Some(row) = stream.try_next().await? {
        let duration = start.elapsed();
        println!("{:?} {:?}", duration, row);
    }

    Ok(())
}
