/// https://doc.rust-lang.org/rust-by-example/mod/split.html
mod foo;
mod foo2015;

use foo::bar::from_inside_bar;
use foo::version;

fn main() {
    foo::version();
    version();

    from_inside_bar();

    foo::bar::from_inside_bar(); // only when pub mod declared
    foo2015::baz::hi_from_baz();
}
