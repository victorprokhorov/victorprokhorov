// mod bar; // visible only to this mod (foo.rs)
pub mod bar; // export, now this mod will be visible outside

pub fn version() {
    println!("2018 style");
}
