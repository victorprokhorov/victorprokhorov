use rand::{rngs::ThreadRng, thread_rng, Rng};

#[derive(Debug, Clone, Copy, PartialEq)]
struct Cell {
    walls: [bool; 4],
}

#[derive(Debug)]
pub struct Maze {
    width: usize,
    height: usize,
    cells: Vec<Cell>,
    thread_rng: ThreadRng,
}

impl Maze {
    pub fn new(width: usize, height: usize) -> Maze {
        let cells = vec![Cell { walls: [true; 4] }; width * height];
        Maze {
            width,
            height,
            thread_rng: thread_rng(),
            cells,
        }
    }
    pub fn paint(&self) {
        let mut i = 0;
        while i < self.width * self.height {
            for k in i..i + self.width {
                if self.cells[k].walls[0] {
                    print!("+++");
                } else {
                    print!("   ");
                }
            }
            println!(" top line");
            for k in i..i + self.width {
                if self.cells[k].walls[3] {
                    print!("+ ");
                } else {
                    print!("  ");
                }
                if self.cells[k].walls[1] {
                    print!("+");
                } else {
                    print!(" ");
                }
            }
            println!(" middle line");
            for k in i..i + self.width {
                if self.cells[k].walls[2] {
                    print!("+++");
                } else {
                    print!("   ");
                }
            }
            println!(" bottom line");
            i += self.width;
        }

        for (i, cell) in self.cells.iter().enumerate() {
            if i % self.width == 0 {
                println!();
            }

            for j in cell.walls {
                print!("{:?}", j);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{Cell, Maze};

    #[test]
    fn new_maze() {
        let maze = Maze::new(4, 4);
        assert_eq!(maze.cells.capacity(), 16);
        maze.cells.iter().for_each(|&cell| {
            assert_eq!(cell, Cell { walls: [true; 4] });
        })
    }
}
