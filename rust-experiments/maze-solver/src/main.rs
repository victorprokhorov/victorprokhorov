use maze_solver::Maze;
use rand::{rngs::ThreadRng, thread_rng, Rng};

// const WIDTH: usize = 4;
// const HEIGHT: usize = 6;
// [
// [****],
// [****],
// [****],
// [****],
// [****],
// [****],
// ]

// #[derive(Clone, Copy, PartialEq, Debug)]
// struct Cell {
//     row: usize,
//     column: usize,
// }
//
// impl Cell {
//     fn from(row: usize, column: usize) -> Cell {
//         Cell { row, column }
//     }
// }
//
// struct Maze {
//     cells: [[bool; WIDTH]; HEIGHT],
//     walls_h: [[bool; WIDTH]; HEIGHT + 1], //horizontal walls existing/removed
//     walls_v: [[bool; WIDTH + 1]; HEIGHT], //vertical walls existing/removed
//     thread_rng: ThreadRng,                //Random numbers generator
// }
//
// impl Maze {
//     ///Inits the maze, with all the cells unvisited and all the walls active
//     fn new() -> Maze {
//         Maze {
//             cells: [[true; WIDTH]; HEIGHT],
//             walls_h: [[true; WIDTH]; HEIGHT + 1],
//             walls_v: [[true; WIDTH + 1]; HEIGHT],
//             thread_rng: thread_rng(),
//         }
//     }
//
//     ///Randomly chooses the starting cell
//     fn first(&mut self) -> Cell {
//         Cell::from(
//             self.thread_rng.gen_range(0..WIDTH),
//             self.thread_rng.gen_range(0..HEIGHT),
//         )
//     }
//
//     ///Opens the enter and exit doors
//     fn open_doors(&mut self) {
//         let from_top: bool = self.thread_rng.gen();
//         let limit = if from_top { WIDTH } else { HEIGHT };
//         let door = self.thread_rng.gen_range(0..limit);
//         let exit = self.thread_rng.gen_range(0..limit);
//         if from_top {
//             self.walls_h[0][door] = false;
//             self.walls_h[HEIGHT][exit] = false;
//         } else {
//             self.walls_v[door][0] = false;
//             self.walls_v[exit][WIDTH] = false;
//         }
//     }
//
//     ///Removes a wall between the two Cell arguments
//     fn remove_wall(&mut self, cell1: &Cell, cell2: &Cell) {
//         // horizontal
//         if cell1.row == cell2.row {
//             //                      which wall we remove
//             self.walls_v[cell1.row][if cell1.column > cell2.column {
//                 cell1.column
//             } else {
//                 cell2.column
//             }] = false; // disable
//         } else {
//             // vertical
//             self.walls_h[if cell1.row > cell2.row {
//                 cell1.row
//             } else {
//                 cell2.row
//             }][cell1.column] = false; // disable
//         };
//     }
//
//     ///Returns a random non-visited neighbor of the Cell passed as argument
//     fn neighbor(&mut self, cell: &Cell) -> Option<Cell> {
//         self.cells[cell.column][cell.row] = false;
//         let mut neighbors = Vec::new();
//
//         if cell.column > 0 && self.cells[cell.column - 1][cell.row] {
//             neighbors.push(Cell::from(cell.column - 1, cell.row));
//         }
//         if cell.row > 0 && self.cells[cell.column][cell.row - 1] {
//             neighbors.push(Cell::from(cell.column, cell.row - 1));
//         }
//         if cell.column < WIDTH - 1 && self.cells[cell.column + 1][cell.row] {
//             neighbors.push(Cell::from(cell.column + 1, cell.row));
//         }
//         if cell.row < HEIGHT - 1 && self.cells[cell.column][cell.row + 1] {
//             neighbors.push(Cell::from(cell.column, cell.row + 1));
//         }
//         if neighbors.is_empty() {
//             None
//         } else {
//             let next = neighbors
//                 .get(self.thread_rng.gen_range(0..neighbors.len()))
//                 .unwrap();
//
//             self.remove_wall(cell, next); // here we remove the wall
//                                           //      between  ^ and ^
//
//             Some(*next)
//         }
//     }
//
//     ///Builds the maze (runs the Depth-first search algorithm)
//     fn build(&mut self) {
//         let mut cell_stack: Vec<Cell> = Vec::new(); // empty
//         let mut next = self.first(); // randomly pick one node
//
//         println!("cell_stack: {:?}, next: {:?}", cell_stack, next);
//
//         loop {
//             while let Some(cell) = self.neighbor(&next) {
//                 // we remove the wall here
//                 // return random node in range
//                 println!("neighbor cell: {:?}", cell);
//                 cell_stack.push(cell);
//                 next = cell; // repeat same process with next cell
//             }
//             match cell_stack.pop() {
//                 Some(cell) => {
//                     println!(
//                         "cell_stack cell: {:?} <- to be poped to continue searching non visited",
//                         cell
//                     );
//                     next = cell;
//                 }
//                 None => break,
//             }
//         }
//     }
//
//     ///MAZE SOLVING: Find the starting cell of the solution
//     fn solution_first(&self) -> Option<Cell> {
//         for (i, wall) in self.walls_h[0].iter().enumerate() {
//             if !wall {
//                 return Some(Cell::from(i, 0));
//             }
//         }
//         for (i, wall) in self.walls_v.iter().enumerate() {
//             if !wall[0] {
//                 return Some(Cell::from(0, i));
//             }
//         }
//         None
//     }
//
//     ///MAZE SOLVING: Find the last cell of the solution
//     fn solution_last(&self) -> Option<Cell> {
//         for (i, wall) in self.walls_h[HEIGHT].iter().enumerate() {
//             if !wall {
//                 return Some(Cell::from(i, HEIGHT - 1));
//             }
//         }
//         for (i, wall) in self.walls_v.iter().enumerate() {
//             if !wall[WIDTH] {
//                 return Some(Cell::from(WIDTH - 1, i));
//             }
//         }
//         None
//     }
//
//     ///MAZE SOLVING: Get the next candidate cell
//     fn solution_next(&mut self, cell: &Cell) -> Option<Cell> {
//         self.cells[cell.column][cell.row] = false;
//         let mut neighbors = Vec::new();
//
//         // columnumn is inside the maze                          and not the wall
//         if cell.column > 0
//             && self.cells[cell.column - 1][cell.row]
//             && !self.walls_v[cell.row][cell.column]
//         {
//             neighbors.push(Cell::from(cell.column - 1, cell.row));
//         }
//         // row is inside the maze                             and not the wall
//         if cell.row > 0
//             && self.cells[cell.column][cell.row - 1]
//             && !self.walls_h[cell.row][cell.column]
//         {
//             neighbors.push(Cell::from(cell.column, cell.row - 1));
//         }
//
//         if cell.column < WIDTH - 1
//             && self.cells[cell.column + 1][cell.row]
//             && !self.walls_v[cell.row][cell.column + 1]
//         {
//             neighbors.push(Cell::from(cell.column + 1, cell.row));
//         }
//         if cell.row < HEIGHT - 1
//             && self.cells[cell.column][cell.row + 1]
//             && !self.walls_h[cell.row + 1][cell.column]
//         {
//             neighbors.push(Cell::from(cell.column, cell.row + 1));
//         }
//         if neighbors.is_empty() {
//             None
//         } else {
//             let next = neighbors
//                 .get(self.thread_rng.gen_range(0..neighbors.len())) // pick random node
//                 .unwrap();
//             Some(*next)
//         }
//     }
//
//     ///MAZE SOLVING: solve the maze
//     ///Uses self.cells to store the solution cells (true)
//     fn solve(&mut self) {
//         // println!("{:?}", self.cells); // all false
//         self.cells = [[true; WIDTH]; HEIGHT]; // why init again?
//                                               // println!("{:?}", self.cells); // all true
//         let mut solution: Vec<Cell> = Vec::new();
//         let mut next = self.solution_first().unwrap(); // aka root node or current node
//         solution.push(next);
//         let last = self.solution_last().unwrap(); // aka objective
//
//         'main: loop {
//             while let Some(cell) = self.solution_next(&next) {
//                 solution.push(cell);
//                 if cell == last {
//                     // found
//                     break 'main;
//                 }
//                 next = cell; // move on
//             }
//             solution.pop().unwrap();
//             next = *solution.last().unwrap();
//         }
//         self.cells = [[false; WIDTH]; HEIGHT]; // again we reset probabaly better have anopther system
//         for cell in solution {
//             self.cells[cell.column][cell.row] = true;
//         }
//     }
//
//     ///MAZE SOLVING: Ask if cell is part of the solution (cells[column][row] == true)
//     fn is_solution(&self, column: usize, row: usize) -> bool {
//         self.cells[column][row]
//     }
//
//     ///Displays a wall
//     ///MAZE SOLVING: Leave space for printing '*' if cell is part of the solution
//     /// (only when painting vertical walls)
//     ///
//     // fn paint_wall(h_wall: bool, active: bool) {
//     //     if h_wall {
//     //         print!("{}", if active { "+---" } else { "+   " });
//     //     } else {
//     //         print!("{}", if active { "|   " } else { "    " });
//     //     }
//     // }
//     fn paint_wall(h_wall: bool, active: bool, with_solution: bool) {
//         if h_wall {
//             print!("{}", if active { "+---" } else { "+   " });
//         } else {
//             print!(
//                 "{}{}",
//                 if active { "|" } else { " " },
//                 if with_solution { "" } else { "   " }
//             );
//         }
//     }
//
//     ///MAZE SOLVING: Paint * if cell is part of the solution
//     fn paint_solution(is_part: bool) {
//         print!("{}", if is_part { " * " } else { "   " });
//     }
//
//     ///Displays a final wall for a row
//     fn paint_close_wall(h_wall: bool) {
//         if h_wall {
//             println!("+")
//         } else {
//             println!()
//         }
//     }
//
//     ///Displays a whole row of walls
//     ///MAZE SOLVING: Displays a whole row of walls and, optionally, the included solution cells.
//     // fn paint_row(&self, h_walls: bool, index: usize) {
//     //     let iter = if h_walls { self.walls_h[index].iter() } else { self.walls_v[index].iter() };
//     //     for &wall in iter {
//     //         Maze::paint_wall(h_walls, wall);
//     //     }
//     //     Maze::paint_close_wall(h_walls);
//     // }
//     fn paint_row(&self, h_walls: bool, index: usize, with_solution: bool) {
//         let iter = if h_walls {
//             self.walls_h[index].iter()
//         } else {
//             self.walls_v[index].iter()
//         };
//         for (column, &wall) in iter.enumerate() {
//             Maze::paint_wall(h_walls, wall, with_solution);
//             if !h_walls && with_solution && column < WIDTH {
//                 Maze::paint_solution(self.is_solution(column, index));
//             }
//         }
//         Maze::paint_close_wall(h_walls);
//     }
//
//     ///Paints the maze
//     ///MAZE SOLVING: Displaying the solution is an option
//     // fn paint(&self) {
//     //     for i in 0 .. HEIGHT {
//     //         self.paint_row(true, i);
//     //         self.paint_row(false, i);
//     //     }
//     //     self.paint_row(true, HEIGHT);
//     // }
//     fn paint(&self, with_solution: bool) {
//         for i in 0..HEIGHT {
//             self.paint_row(true, i, with_solution);
//             self.paint_row(false, i, with_solution);
//         }
//         self.paint_row(true, HEIGHT, with_solution);
//     }
// }

fn main() {
    let mut maze = Maze::new(4, 4);
    // println!("New maze:");
    maze.paint();



    // maze.build();
    // println!("The maze build:");
    // maze.paint(true);
    //
    // maze.open_doors();
    // println!("The maze doors open:");
    // maze.paint(false);
    //
    // maze.solve();
    // println!("The maze, solved:");
    // maze.paint(true);
}
