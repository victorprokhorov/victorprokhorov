<https://yew.rs/docs/getting-started/introduction>

<https://yew.rs/docs/tutorial>

<https://github.com/emilk/egui>

<https://rustwasm.github.io/docs/book/>

<https://developer.mozilla.org/en-US/docs/WebAssembly/Rust_to_wasm>

# notes from rustwasm book
TODO: add Dockerfile
TODO: detect stable system
TODO: after the tut check if it worth to add this to generic +x bootstrap sh  
<https://rustwasm.github.io/wasm-pack/installer/>  
curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh  
cargo install cargo-generate  
https://github.com/rustwasm/create-wasm-app  
npm init wasm-app www  
example or wasm without a bundler:  
https://rustwasm.github.io/docs/wasm-bindgen/examples/without-a-bundler.html
npm i -g webpack-dev-server
npm install -D webpack-cli
on node 18:
export NODE_OPTIONS=--openssl-legacy-provider
fuzzing
cargo install afl 
cargo new --bin url-fuzz-target
https://rust-fuzz.github.io/book/cargo-fuzz/tutorial.html
libFuzzer is the recomended way of fuzzing
cargo install cargo-fuzz
cargo fuzz init
cargo fuzz list

// NOTE: benchmaring
// https://doc.rust-lang.org/unstable-book/library-features/test.html
// run:
// NOTE: tee is for T shaped pipe - it send to both STDOUT and the file
// thats the difference with > (write or overwrite) or >> (append)
// cargo bench | tee before.txt
// https://stackoverflow.com/a/60276918
// sudo cp perf /usr/local/bin/
// perf record -g target/release/deps/bench-8474091a05cfa2d9 --bench
// perf report
// cargo bench | tee after.txt
//
// cargo install cargo-benchcmp
// cargo benchcmp before.txt after.txt
//
