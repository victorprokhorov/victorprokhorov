use fixedbitset::FixedBitSet;

fn main() {
    // let mut bs = FixedBitSet::new(); // insert at index 1 exceeds fixbitset size 0, grow then
    // insert if starting with new
    // let a: [u8; 4] = [0, 1, 0, 1];
    let mut bs = FixedBitSet::with_capacity(4);
    println!("{:?}", bs);
    assert_eq!(format!("{:b}", bs), "0000");
    bs.insert(0); // insert 1 at the position
    let x = bs.put(1); // enable bit and return previous bit
    assert_eq!(x, false); // returned value is bool
    bs.set(2, true); // (position of the bit, value to set)
    assert_eq!(format!("{:b}", bs), "1110");
    assert!(bs[0]);
    assert!(bs[1]);
    assert!(bs[2]);
    assert!(!bs[3]);
    // out of bound?
    assert!(!bs[666]); // don't panick
}
