# Usage

## Build Docker image and start the container

```
./scripts/run.sh
```

## Develop locally

```
./scripts/develop.sh
```

## Compare implementations

```
./scripts/bench.sh
```

## Fuzzing

```
./scripts/fuzz.sh
```
