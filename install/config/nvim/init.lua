vim.cmd('set nocompatible') 
vim.cmd('set clipboard=unnamed') 
vim.cmd("set mouse=a")
vim.cmd('set tabstop=4') 
vim.cmd('set shiftwidth=4') 
vim.cmd('set softtabstop=4') 
vim.cmd('set expandtab') 
vim.cmd('set number') 
vim.cmd('set termguicolors')
vim.cmd('let base16colorspace=256')
vim.cmd('set noswapfile') 
vim.cmd('set nobackup') 
vim.cmd('set scrolloff=1')
vim.cmd('set nowrap')
vim.cmd('set incsearch')
vim.cmd('set ignorecase')
vim.cmd('set smartcase')
vim.cmd('set showmatch')
vim.cmd('set nohlsearch')
vim.cmd('set wildmenu')
vim.cmd('set wildmode=list:longest')
vim.cmd('syntax enable')
vim.cmd('filetype plugin indent on')
vim.cmd("set updatetime=150" )
vim.cmd("set signcolumn=yes" )

require('packer').startup(function(use)
    use 'wbthomason/packer.nvim'
    use 'chriskempson/base16-vim'
    -- use {
    --     'nvim-treesitter/nvim-treesitter',
    --     run = function() require('nvim-treesitter.install').update({ with_sync = true }) end,
    -- }
    use 'neovim/nvim-lspconfig' -- :help lspconfig-all
    use "hrsh7th/cmp-nvim-lsp"
    use "hrsh7th/cmp-buffer"
    use "hrsh7th/cmp-path"
    use "hrsh7th/cmp-cmdline"
    use "hrsh7th/nvim-cmp"
    use "hrsh7th/cmp-vsnip"
    use "hrsh7th/vim-vsnip"
    use "petertriho/cmp-git"
    use 'rust-lang/rust.vim'
    use "junegunn/fzf"
    use "junegunn/fzf.vim"
    use "tpope/vim-surround"
    use "Townk/vim-autoclose"
end)

-- require'nvim-treesitter.configs'.setup {
-- ensure_installed = { "c", "bash", "lua", "rust" },
-- sync_install = false,
-- auto_install = true,
-- highlight = {
--   enable = true,
--   additional_vim_regex_highlighting = false,
-- },
-- indent = {
--    enable = true,
-- },
-- matchup = { 
--    enable = true,
-- }
-- }
--()
vim.cmd('colorscheme base16-gruvbox-dark-hard') 
vim.cmd [[
hi DiagnosticError guifg=White
hi DiagnosticWarn  guifg=White
hi DiagnosticInfo  guifg=White
hi DiagnosticHint  guifg=White
]]
vim.cmd [[
    set noshowmatch
]]

local opts = { noremap=true, silent=true }
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, opts)

local on_attach = function(client, bufnr)
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

  local bufopts = { noremap=true, silent=true, buffer=bufnr }
  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
  vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
  vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
  vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, bufopts)
  vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, bufopts)
  vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, bufopts)
  vim.keymap.set('n', '<space>ca', vim.lsp.buf.code_action, bufopts)
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
  vim.keymap.set('n', '<space>f', vim.lsp.buf.formatting, bufopts)
end

require('lspconfig')['rust_analyzer'].setup{
    on_attach = on_attach,
    flags = lsp_flags,
    settings = {
       ["rust-analyzer"] = {
           imports = {
               granularity = {
                   group = "module",
               },
               prefix = "self",
           },
           cargo = {
               allFeatures = true
           },
           completion = {
               postfix = {
                   enable = false
               }
           },
           checkOnSave = {
               command = "clippy"
           },
           procMacro = {
               enable = true
           },
       }
    }
} 


local cmp = require "cmp"

-- cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done())

cmp.setup(
    {
        snippet = {
            expand = function(args)
                vim.fn["vsnip#anonymous"](args.body)
            end
        },
        mapping = {
            -- ["<Tab>"] = cmp.mapping.confirm({select = true}),
            ["<TAB>"] = cmp.mapping(
                {
                    i = cmp.mapping.confirm({behavior = cmp.ConfirmBehavior.Insert, select = true}),
                    c = cmp.mapping(cmp.mapping.select_next_item())
                }
            ),
            ["<C-p>"] = cmp.mapping.select_prev_item(),
            ["<C-n>"] = cmp.mapping.select_next_item()
        },
        -- formatting = {
        -- fields = { 'menu', 'abbr', 'kind'}
        -- },
        sources = cmp.config.sources(
            {
                {name = "nvim_lsp"},
                {name = "vsnip"},
                {name = "path"},
                {name = "buffer"}
            }
            -- {
            --
            -- }
        ),
        experimental = {
            -- native_menu = false,
            -- ghost_text = true
        }
    }
)

-- Set configuration for specific filetype.
-- cmp.setup.filetype(
--     "gitcommit",
--     {
--         sources = cmp.config.sources(
--             {
--                 {name = "cmp_git"}
--             },
--             {{name = "buffer"}}
--         )
--     }
-- )

cmp.setup.cmdline(
    "/",
    {
        mapping = cmp.mapping.preset.cmdline(),
        sources = {{name = "buffer"}}
    }
)

cmp.setup.cmdline(
    ":",
    {
        mapping = cmp.mapping.preset.cmdline(),
        sources = cmp.config.sources({{name = "path"}}, {{name = "cmdline"}})
    }
)


-- vim.o.list = false
-- 
-- vim.o.listchars = [[tab:→\ ,space:·,nbsp:␣,trail:·,eol:¬,precedes:«,extends:»]]
-- 
-- vim.api.nvim_create_autocmd("ModeChanged", {
--     pattern = "*:[vV\x16]*",
--     group =  group,
--     callback = function()
--       vim.o.list = true
--     end,
--   } 
-- )
-- 
-- vim.api.nvim_create_autocmd("ModeChanged", {
--     pattern = "[vV\x16]*:*",
--     group = group,
--     callback = function()
--       vim.o.list = false
--     end,
--   }
-- )




-- nmap <space> <leader>
vim.cmd [[
nnoremap <SPACE> <Nop>
let mapleader=" "
]] -- the second one disable space movement in normal map so its only leader key 

vim.cmd [[ nmap <space> <leader> ]] 
vim.cmd "nmap <leader>w :w<CR>"
vim.cmd "noremap <leader>s :Rg<CR>"
vim.cmd "nmap <leader>s :Rg<CR>"
