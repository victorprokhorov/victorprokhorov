#!/bin/bash

# sudo apt update \
# 	&& sudo apt install -y \
#   libz-dev \
#   libssl-dev \
#   libcurl4-gnutls-dev \
#   libexpat1-dev \
#   gettext \
#   cmake \
#   gcc
# curl -o git.tar.gz https://mirrors.edge.kernel.org/pub/software/scm/git/git-2.37.3.tar.gz
# tar -zxf git.tar.gz
# rm git.tar.gz
# cd git-*
# make prefix=/usr/local all
# sudo make prefix=/usr/local install
# cd ..
# rm -rf git-*
# 
# git config --global user.name 'Victor Prokhorov'
# git config --global user.email 'mail@victorprokhorov.com'
# git config --global init.defaultBranch main
# 
# mkdir -p $HOME/.local/bin/
# git clone --depth 1 https://github.com/junegunn/fzf.git
# cd fzf
# ./install
# mv bin/* $HOME/.local/bin/
# cd ..
# rm -rf fzf
# 
# sudo apt install -y \
#         ninja-build \
#         gettext \
#         libtool \
#         libtool-bin \
#         autoconf \
#         automake \
#         cmake \
#         g++ \
#         pkg-config \
#         unzip \
#         curl \
#         doxygen \
#         ripgrep
# git clone https://github.com/neovim/neovim
# cd neovim
# git checkout stable
# make CMAKE_BUILD_TYPE=RelWithDebInfo
# sudo make install
# cd ..
# rm -rf neovim
# mkdir -p $HOME/.config/nvim
ln -rsf ./config/nvim/init.lua $HOME/.config/nvim/init.lua
# 
curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -
sudo apt install -y nodejs
mkdir -p $HOME/.npm-global
npm config set prefix "$HOME/.npm-global"
echo 'export PATH=$HOME/.npm-global/bin:$PATH' >> $HOME/.profile
 
# git clone --depth 1 https://github.com/wbthomason/packer.nvim \
#	$HOME/.local/share/nvim/site/pack/packer/start/packer.nvim
nvim --headless -c 'autocmd User PackerComplete quitall' -c 'PackerSync'
# TSUpdate

mkdir -p $HOME/.local/bin/rust-analyzer
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --default-toolchain nightly -y
git clone https://github.com/rust-lang/rust-analyzer.git $HOME/.local/bin/rust-analyzer
cd $HOME/.local/bin/rust-analyzer
cargo xtask install --server
# 
# ssh-keygen -t ed25519 -f "$HOME/.ssh/id_ed25519"
# eval "$(ssh-agent -s)"
# ssh-add $HOME/.ssh/id_ed25519
# 
# sudo apt update
# sudo apt install -y \
#   libevent-dev \
#   ncurses-dev \
#   build-essential \
#   bison \
#   pkg-config
# git clone https://github.com/tmux/tmux.git
# cd tmux
# sh autogen.sh
# ./configure
# make && sudo make install
# cd ..
# rm -rf tmux
# mkdir -p $HOME/.config/tmux
ln -rsf ./config/tmux/tmux.conf $HOME/.config/tmux/tmux.conf 
