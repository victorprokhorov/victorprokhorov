find . -type l -xtype d - find dirs that ae symbolik links

ctrl A X inc decrement
tpope vim comment
invisual mode
:norm[al] 
:!sort | - use unix commands on text

:r!ls - will insert output of ls command into file
ctrl O ctrl I walk trough locationhistory backwa fward
:so%

! dip[aragraph] !

=ip  - indent
https://honors.cs128.org/hof
0:49 gj and gk : Move cursor up and down to wrapped part of a line as if it is a different line. 
2:12 g0 and g$ : Same as previous but move cursor to the first and last letter of a wrapped line. 
3:32 gq : Turn a long line into multiple lines. 
4:55 gu and gU : Uncapitalize and capitalize words/lines. 
6:00 ~ and g~ : Switch capitalization of a letter. 

! 7:02 gf : Open highlighted text as file. (gx same for url) ! 


you can search a text or patern with "/" then do "cgn" to change it to something different.
then typing "cgn" again will jump to the next occurence and change it.
you can then type "." to repeat as much as you want.
or do something like "3." or "3cgn".
Docker mmemos,
copy over add
docker build -t fancyname:6.66 . -- tag is for image
-it for interactive
-p 8080:80 port binding
docker run --name containerinstance fancyname -- name is for container
you can `cd $h` for lazy home nav
https://cheatsheetseries.owasp.org/cheatsheets/Session_Management_Cheat_Sheet.html
https://redis.com/blog/json-web-tokens-jwt-are-dangerous-for-user-sessions/
:enew - will open new empty unamed buffer 
cargo install cargo-watch -- seems to be equivalent to nodemon in node ecosys
-c clear the screen -x the cmd to exec
cargo watch -c -x -w 'src/bin' run --bin my_cool_app'
https://everything.curl.dev/http/cheatsheet
-d @file -- post from file
TODO: split into separate file by tech, git, linux, ts, rs, etc.
- Alt-c - cd into selected dir 
-  fzf, cd **<TAB>
- kill -9 **<TAB>
- C-r replace default bash autocomplete with fzf

* setup autocompletion for renaming from current buffer example:
word1234
:%s/w[tab] -> should autocomplete :%s/word1234
* check for more convenient autocompletion in bash somethig similar to fish
TMUX
C b - tab, then C+0 or C+1 to switch
C+l - toggle between windows
control b " - hortizontal split
C % - split v
C+o - toggle between splits
simply C+d to exit
git add -A - add all file to the repo not only current folder as git add . do
DEBUG_PRINT_LIMIT=10000 yarn test
LINUX
du -cksh * | sort -rn | head
find . -name 'node_modules' -type d -exec rm -r '{}' \;
find . -iname '*pattern*' - case insensitive search file containing pattern
grep -Ril 'pattern' . - search pattern from current folder
(need exclude for node_modules)
grep -Rl 'console.log' src/ | xargs -n1 -I {} sh -c "sed -i '/console.log/d' {}" -- remove console logs
kill $(lsof -ti:6017) -- kill process
VIM
vim -b filename
:%!xxd
:%!xxd -b | -bits switch to binary digits dump rather then hexdump
Backspace delete the character before the cursor
Ctrl+h also deletes the character before the cursor
Ctrl+w delete characters before the cursor until start of a word
Ctrl+u delete all the characters before the cursor in the current line, preserves indentation if any same as in bash for C+w & C+u
:g/^\s*$/d - Remove all blank lines
:ls - list buffers
:b0 - checkout buf 0
:Buffers via fzf give nice preview and can be C+p / C+n
:bd[elete] - close buffer
C+6 - toggle between 2 last buffers
:bp[revious]
:bn[ext]
S' - surround with ' in visual mode
cst<tag - change tag to tag
cstt - change surrounding tag by tag
C + z - run in background or :sus[pend]
fg - run in foreground
jobs - list background jobs
C + w, v
C + w, w
:split :sp <file> :vsp <file>
:on[ly]
:tabnew
:tabonly :tabo[nly]
gt gT (tab)
:wqa
:e <file>
:%s/<text>/<text>/gc
m<letter> - save mark
'<letter> - move to mark
"1p - paste last deletion
"<letter><action>
"a3dd - delete 3 lines to named buffer a
"ap - paste from buffer a
GITLAB
on merge request remove branch and squash commits
YARN
yarn build
yarn link
PACKAGES
npx npm-check-updates -u @your_org/*
GIT
git checkout --track origin/<branch> - checkout new branch from remote
git reset --hard main
git log --oneline remote1/branch..remote2/branch
git add --patch - NEED git-perl git-email
git add -p

git checkout --ours -- <file>
git checkout --theirs -- <file>
git branch --contains <hash>
diff --brief --recursive <dir> <dir>
cp -TRv <div> <div>
git push -d <remove=origin> <branch>
git branch -d <branch>
git revert <commit-hash-to-revert>
git revert -m 1 <merge-commit-hash>
git commit --no-verify
git push --no-verify
git log <branch> -n <number>
git merge feature main - checkout feature and merge main oneliner
git checkout feature git rebase main - linear history but rewrite history
git push --force origin main
git fetch <remote>
git branch -v -a
git switch -c <branch> <remote>/<branch>
git switch -c <non_existing_branch>
is the same as but avoid unclear 'checkout' that used for many things
git checkout -b <non_existing_branch>
stacks memo:
https://www.conventionalcommits.org/en/v1.0.0/
https://commitizen-tools.github.io/commitizen/
https://www.npmjs.com/package/lint-staged
https://www.npmjs.com/package/husky
https://jestjs.io/
https://testing-library.com/
https://www.cypress.io/
https://www.elastic.co/observability/application-performance-monitoring
https://12factor.net/config
https://www.drycode.io/
https://kentcdodds.com/blog/common-mistakes-with-react-testing-library
* https://github.com/rollup/rollup - https://rollupjs.org/guide/en/
* jenkins
* https://www.microfocus.com/en-us/products/alm-octane/overview
* https://www.sonatype.com/products/lifecycle-demo nexus repository iq server
* https://www.sonarqube.org/
* https://www.microfocus.com/fr-fr/products/static-code-analysis-sast/overview
* https://cddirector.io/cdd/sign-in.jsp
* https://yeoman.io/ website scaffolding tool
* webpack
