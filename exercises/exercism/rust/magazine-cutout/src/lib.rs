#![warn(clippy::pedantic)]
use std::collections::HashMap;

#[must_use]
pub fn can_construct_note(magazine: &[&str], note: &[&str]) -> bool {
    let mut map: HashMap<&str, u8> = HashMap::new();

    for i in magazine {
        *map.entry(i).or_insert(0) += 1;
    }

    for i in note {
        match map.get_mut(i) {
            Some(x) if *x > 0 => {
                *x -= 1;
            }
            _ => return false,
        }
    }

    true
}
