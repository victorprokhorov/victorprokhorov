#![feature(test)]

extern crate magazine_cutout;
extern crate test;

#[bench]
fn tick(b: &mut test::Bencher) {
    b.iter(|| {
            let magazine = "two times three is not four"
                .split_whitespace()
                .collect::<Vec<&str>>();
            let note = "two times two is four"
                .split_whitespace()
                .collect::<Vec<&str>>();
            assert!(!magazine_cutout::can_construct_note(&magazine, &note));
        
            let magazine = "The metro orchestra unveiled its new grand piano today. Its donor paraphrased Nathn Hale: \"I only regret that I have but one to give \"".split_whitespace().collect::<Vec<&str>>();
            let note = "give one grand today."
                .split_whitespace()
                .collect::<Vec<&str>>();
            assert!(magazine_cutout::can_construct_note(&magazine, &note));
        
            let magazine = "I've got a lovely bunch of coconuts."
                .split_whitespace()
                .collect::<Vec<&str>>();
            let note = "I've got som coconuts"
                .split_whitespace()
                .collect::<Vec<&str>>();
            assert!(!magazine_cutout::can_construct_note(&magazine, &note));
        
            let magazine = "i've got some lovely coconuts"
                .split_whitespace()
                .collect::<Vec<&str>>();
            let note = "I've got some coconuts"
                .split_whitespace()
                .collect::<Vec<&str>>();
            assert!(!magazine_cutout::can_construct_note(&magazine, &note));
        
            let magazine = "I've got some lovely coconuts"
                .split_whitespace()
                .collect::<Vec<&str>>();
            let note = "i've got some coconuts"
                .split_whitespace()
                .collect::<Vec<&str>>();
            assert!(!magazine_cutout::can_construct_note(&magazine, &note));
        
            let magazine = "Enough is enough when enough is enough"
                .split_whitespace()
                .collect::<Vec<&str>>();
            let note = "enough is enough".split_whitespace().collect::<Vec<&str>>();
            assert!(magazine_cutout::can_construct_note(&magazine, &note));
            let magazine = "A A A".split_whitespace().collect::<Vec<&str>>();
            let note = "A nice day".split_whitespace().collect::<Vec<&str>>();
            assert!(!magazine_cutout::can_construct_note(&magazine, &note));
    });
}
