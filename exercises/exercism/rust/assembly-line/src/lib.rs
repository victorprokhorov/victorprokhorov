#![warn(clippy::pedantic)]
#![feature(stmt_expr_attributes)]

#[must_use]
pub fn production_rate_per_hour(speed: u8) -> f64 {
    match speed {
        1..=4 => 221f64 * f64::from(speed),
        5..=8 => 221f64 * f64::from(speed) - (221f64 * f64::from(speed)) * 10f64 / 100f64,
        9..=10 => 221f64 * f64::from(speed) - (221f64 * f64::from(speed)) * 23f64 / 100f64,
        _ => 0.0,
    }
}

#[must_use]
pub fn working_items_per_minute(speed: u8) -> u32 {
    #[allow(clippy::cast_possible_truncation, clippy::cast_sign_loss)]
    (production_rate_per_hour(speed) as u32)
        / 60
}
