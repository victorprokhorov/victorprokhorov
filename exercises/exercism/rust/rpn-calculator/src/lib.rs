#![warn(clippy::pedantic)]

#[derive(Debug)]
pub enum CalculatorInput {
    Add,
    Subtract,
    Multiply,
    Divide,
    Value(i32),
}

#[must_use]
pub fn evaluate(inputs: &[CalculatorInput]) -> Option<i32> {
    let mut stack: Vec<i32> = Vec::new();

    for input in inputs {
        match input {
            CalculatorInput::Value(number) => stack.push(*number),
            CalculatorInput::Add => {
                let r = stack.pop()?;
                let l = stack.pop()?;

                stack.push(l + r);
            }
            CalculatorInput::Subtract => {
                let r = stack.pop()?;
                let l = stack.pop()?;

                stack.push(l - r);
            }
            CalculatorInput::Multiply => {
                let r = stack.pop()?;
                let l = stack.pop()?;

                stack.push(l * r);
            }
            CalculatorInput::Divide => {
                let r = stack.pop()?;
                let l = stack.pop()?;

                stack.push(l / r);
            }
        }
    }
    if stack.len() == 1 {
        stack.pop()
    } else {
        None
    }
}
