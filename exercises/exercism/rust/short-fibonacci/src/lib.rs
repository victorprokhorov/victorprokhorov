#![warn(clippy::pedantic)]

#[must_use]
pub fn create_empty() -> Vec<u8> {
    Vec::new()
}

#[must_use]
pub fn create_buffer(count: usize) -> Vec<u8> {
    vec![0; count]
}

#[must_use]
pub fn fibonacci() -> Vec<u8> {
    let count = 5;
    let mut buffer = create_buffer(count);
    buffer[0] = 1;
    buffer[1] = 1;
    for i in 2..count {
        buffer[i] = buffer[i - 2] + buffer[i - 1];
    }
    buffer
}
