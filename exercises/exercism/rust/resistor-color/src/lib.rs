#![warn(clippy::pedantic)]

use std::convert::{TryFrom, TryInto};
use std::iter::Iterator;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
#[repr(usize)]
pub enum ResistorColor {
    Black = 0,
    Brown = 1,
    Red = 2,
    Orange = 3,
    Yellow = 4,
    Green = 5,
    Blue = 6,
    Violet = 7,
    Grey = 8,
    White = 9,
}

impl TryFrom<usize> for ResistorColor {
    type Error = String;

    fn try_from(value: usize) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(ResistorColor::Black),
            1 => Ok(ResistorColor::Brown),
            2 => Ok(ResistorColor::Red),
            3 => Ok(ResistorColor::Orange),
            4 => Ok(ResistorColor::Yellow),
            5 => Ok(ResistorColor::Green),
            6 => Ok(ResistorColor::Blue),
            7 => Ok(ResistorColor::Violet),
            8 => Ok(ResistorColor::Grey),
            9 => Ok(ResistorColor::White),
            _ => Err("value out of range".to_string()),
        }
    }
}

// impl Iterator for ResistorColor {
//     type Item = ResistorColor;

//     fn next(&mut self) -> Option<Self::Item> {
//         let next_value = *self as usize + 1;
//         match <usize as TryInto<ResistorColor>>::try_into(next_value) {
//             Ok(color) => Some(color),
//             Err(_) => None,
//         }
//     }
// }

impl IntoIterator for ResistorColor {
    type Item = ResistorColor;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        let mut colors = vec![];
        while let Ok(color) = <usize as TryInto<ResistorColor>>::try_into(self as usize) {
            colors.push(color);
        }
        colors.into_iter()
    }
}

#[must_use]
pub fn color_to_value(color: ResistorColor) -> usize {
    color as usize
}

#[must_use]
pub fn value_to_color_string(value: usize) -> String {
    value
        .try_into()
        .map_or_else(|e| e, |color: ResistorColor| format!("{color:?}"))
}

#[must_use]
pub fn colors() -> Vec<ResistorColor> {
    ResistorColor::into_iter(
        <usize as TryInto<ResistorColor>>::try_into(0).expect("at least one correct value"),
    )
    .collect()
}
