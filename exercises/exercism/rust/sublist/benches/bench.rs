#![feature(test)]

extern crate sublist;
extern crate test;
use sublist::{alireza4050s_sublist, rsalmeis_sublist, sublist, Comparison};

fn test<F>(sublist: F)
where
    F: Fn(&[u64], &[u64]) -> Comparison,
{
    let v: &[u64] = &[];
    assert_eq!(Comparison::Equal, sublist(v, v));
    assert_eq!(Comparison::Sublist, sublist(&[], &[1, 2, 3]));
    assert_eq!(Comparison::Superlist, sublist(&[1, 2, 3], &[]));
    assert_eq!(Comparison::Unequal, sublist(&[1], &[2]));
    use std::iter::repeat;
    let v: Vec<u64> = repeat(1).take(1000).collect();
    assert_eq!(Comparison::Equal, sublist(&v, &v));
    assert_eq!(Comparison::Sublist, sublist(&[1, 2, 3], &[1, 2, 3, 4, 5]));
    assert_eq!(Comparison::Sublist, sublist(&[4, 3, 2], &[5, 4, 3, 2, 1]));
    assert_eq!(Comparison::Sublist, sublist(&[3, 4, 5], &[1, 2, 3, 4, 5]));
    assert_eq!(Comparison::Sublist, sublist(&[1, 1, 2], &[1, 1, 1, 2]));
    let huge: Vec<u64> = (1..1_000_000).collect();
    assert_eq!(Comparison::Sublist, sublist(&[3, 4, 5], &huge));
    let v1: Vec<u64> = (10..1_000_001).collect();
    let v2: Vec<u64> = (1..1_000_000).collect();
    assert_eq!(Comparison::Unequal, sublist(&v1, &v2));
    assert_eq!(Comparison::Superlist, sublist(&[1, 2, 3, 4, 5], &[1, 2, 3]));
    assert_eq!(Comparison::Superlist, sublist(&[5, 4, 3, 2, 1], &[4, 3, 2]));
    assert_eq!(Comparison::Superlist, sublist(&[1, 2, 3, 4, 5], &[3, 4, 5]));
    assert_eq!(Comparison::Unequal, sublist(&[1, 2, 3], &[1, 3]));
    let huge: Vec<u64> = (1..1_000_000).collect();
    assert_eq!(Comparison::Superlist, sublist(&huge, &[3, 4, 5]));
    assert_eq!(
        Comparison::Sublist,
        sublist(&[1, 2, 1, 2, 3], &[1, 2, 3, 1, 2, 1, 2, 3, 2, 1])
    );
    assert_eq!(
        Comparison::Unequal,
        sublist(&[1, 2, 1, 2, 3], &[1, 2, 3, 1, 2, 3, 2, 3, 2, 1])
    );
}

#[bench]
fn bench1(b: &mut test::Bencher) {
    b.iter(|| test(sublist));
}

#[bench]
fn bench2(b: &mut test::Bencher) {
    b.iter(|| test(rsalmeis_sublist));
}

#[bench]
fn bench3(b: &mut test::Bencher) {
    b.iter(|| test(alireza4050s_sublist));
}
