#![warn(clippy::pedantic)]

#[derive(Debug, PartialEq, Eq)]
pub enum Comparison {
    Equal,
    Sublist,
    Superlist,
    Unequal,
}

// 5,955,960 ns/iter
pub fn sublist<T: PartialEq + std::fmt::Debug + std::marker::Copy>(
    first_list: &[T],
    second_list: &[T],
) -> Comparison {
    if first_list == second_list {
        return Comparison::Equal;
    }

    if first_list.is_empty() && !second_list.is_empty() {
        return Comparison::Sublist;
    }
    if !first_list.is_empty() && second_list.is_empty() {
        return Comparison::Superlist;
    }

    let is_sublist = |first_list: &[T], second_list: &[T]| {
        for (i, t) in second_list.iter().enumerate() {
            if t == first_list
                .first()
                .expect("logical error: list should not be empty")
                && first_list.len().saturating_add(i) < second_list.len() + 1
                && first_list == &second_list[i..first_list.len().saturating_add(i)]
            {
                return true;
            }
        }

        false
    };

    if is_sublist(first_list, second_list) {
        return Comparison::Sublist;
    }
    if is_sublist(second_list, first_list) {
        return Comparison::Superlist;
    }

    Comparison::Unequal
}

// 4,804,325 ns/iter
pub fn rsalmeis_sublist<T: PartialEq>(first_list: &[T], second_list: &[T]) -> Comparison {
    let superlist = second_list.is_empty()
        || first_list
            .windows(second_list.len())
            .any(|x| x == second_list);

    let sublist = first_list.is_empty()
        || second_list
            .windows(first_list.len())
            .any(|x| x == first_list);

    match (superlist, sublist) {
        (true, true) => Comparison::Equal,
        (true, false) => Comparison::Superlist,
        (false, true) => Comparison::Sublist,
        (false, false) => Comparison::Unequal,
    }
}

// 4,843,070 ns/iter
pub fn alireza4050s_sublist<T: Eq>(a: &[T], b: &[T]) -> Comparison {
    use Comparison::{Equal, Sublist, Superlist, Unequal};

    match (a.len(), b.len()) {
        (0, 0) => Equal,
        (0, _) => Sublist,
        (_, 0) => Superlist,
        (m, n) if m > n => {
            if a.windows(n).any(|v| v == b) {
                Superlist
            } else {
                Unequal
            }
        }
        (m, n) if m < n => {
            if b.windows(m).any(|v| v == a) {
                Sublist
            } else {
                Unequal
            }
        }
        (_, _) => {
            if a == b {
                Equal
            } else {
                Unequal
            }
        }
    }
}
