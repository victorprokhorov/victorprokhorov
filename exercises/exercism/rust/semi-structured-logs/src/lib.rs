#![warn(clippy::pedantic)]

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum LogLevel {
    Info,
    Warning,
    Error,
}

#[must_use]
pub fn log(level: LogLevel, message: &str) -> String {
    let level = format!("{:?}", level).to_uppercase();
    format!("[{}]: {}", level, message)
}

#[must_use]
pub fn info(message: &str) -> String {
    log(LogLevel::Info, message)
}

#[must_use]
pub fn warn(message: &str) -> String {
    log(LogLevel::Warning, message)
}

#[must_use]
pub fn error(message: &str) -> String {
    log(LogLevel::Error, message)
}
