#![warn(clippy::pedantic)]

#[must_use]
pub fn hello() -> &'static str {
    return "Hello, World!";
}
