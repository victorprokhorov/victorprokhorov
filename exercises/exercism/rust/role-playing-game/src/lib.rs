#![warn(clippy::pedantic)]

pub struct Player {
    pub health: u32,
    pub mana: Option<u32>,
    pub level: u32,
}

impl Player {
    #[must_use]
    pub fn new(level: u32) -> Player {
        Player {
            health: 100,
            mana: if level < 10 { None } else { Some(100) },
            level,
        }
    }

    #[must_use]
    pub fn revive(&self) -> Option<Player> {
        match self.health {
            0 => Some(Player::new(self.level)),
            _ => None,
        }
    }

    pub fn cast_spell(&mut self, mana_cost: u32) -> u32 {
        match self.mana {
            Some(x) if x < mana_cost => 0,
            Some(x) => {
                self.mana = Some(x - mana_cost);
                2 * mana_cost
            }
            None => {
                self.health = self.health - std::cmp::min(self.health, mana_cost);
                0
            }
        }
    }
}
