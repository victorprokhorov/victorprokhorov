use std::collections::HashMap;
use std::thread;

fn process(string: String, hash_map: &mut HashMap<char, usize>) {
    string.chars().for_each(|char| {
        if char.is_alphabetic() {
            hash_map
                .entry(char.to_ascii_lowercase())
                .and_modify(|counter| *counter += 1)
                .or_insert(1);
        }
    });
}

pub fn frequency(input: &[&str], worker_count: usize) -> HashMap<char, usize> {
    let hash_map = HashMap::new();

    if input.is_empty() {
        return hash_map;
    }

    let chars = input.join("");

    let mut handles = vec![];
    for _ in 0..worker_count {
        let string: String = chars
            .chars()
            .by_ref()
            .take((input.len() / worker_count).max(1))
            .collect();
        let handle = thread::spawn(move || {
            let mut hash_map: HashMap<char, usize> = HashMap::new();
            process(string, &mut hash_map);
            hash_map
        });
        handles.push(handle);
    }

    for handle in handles {
        let worked_hash_map = handle.join().unwrap();
        for (k, v) in worked_hash_map.iter() {
            
        }

        println!("{hash_map:?}");
    }

    hash_map
}
